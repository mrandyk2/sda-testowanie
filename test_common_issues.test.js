// TRUIZM / bezsensowny opis który nic nie wnosi

const someComponent = {
  /** ... */
};

describe("SomeComponent", () => {
  test("should work properly", () => {
    /** ... */
  });
  test("should have good state", () => {
    /** ... */
  });
  test("should throw error", () => {
    /** ... */
  });
  test("should affect to other state", () => {
    /** ... */
  });
});

// WARTOŚĆ BOJOWA - 0

describe("SomeComponent", () => {
  // Tu trzeba komentować? :D
  test("should be defined", () => {
    // Ewidentnie ktoś chciał poprawić sobie ego ilością testów
    expect(someComponent).toBeDefined();
  });

  // 100 lat temu zakomentowany/zaskipowany i wisi bo po co usunąć/poprawić
  test.skip("", () => {
    //GIVEN
    const someData = {
      /** ... */
    };
    const expectedResult = {
      /** ... */
    };
    //WHEN
    someComponent.performSomeAction(someData);
    //THEN
    // expect(someComponent).toMatchObject(expectedResult);
  });
});

// Problem z doborem asercji
describe("SomeComponent", () => {
  test("should first", () => {
    const person = {
      name: "John",
      age: 13,
    };
    const expectedAge = 13;

    expect(person.age === expectedAge).tobeTruthy();
    // ❌ Komunikat w przypadku błędu będzie taki:

    // expect(received).toBeTruthy()

    // Received: false

    expect(person).toHaveProperty("age", expectedAge);
    // ✅ Komunikat w przypadku błędu komunikat lepiej wskazuje na miejsce błędu czy zamierzenie:

    // expect(received).toHaveProperty(path, value)

    // Expected path: "age"
    // Received path: /** ... */

    // Expected value: 11
    // Received value: /** ... */
  });
});

// Mają wspólne zależności

class Hero {
  // ...

  constructor(name, damage = 1, health = 100, mana = 100) {
    this.name = name;
    this.damage = damage;
    this.health = health;
    this.mana = mana;
  }

  alive = () => this.health > 0;

  attack(enemy) {
    if (enemy.alive() === false) {
      throw new Error("Cant attack dead enemy");
    }

    enemy.health -= this.damage;
  }

  castSpell = (enemy) => {
    if (enemy.alive() === false) {
      throw new Error("Cant attack dead enemy");
    }

    if (this.mana < 10) {
      throw new Error("Too low mana to cast magic spell");
    }

    enemy.health -= this.damage * 2;
    this.mana -= 10;
  };
}

describe("Hero", () => {
  let hero = new Hero("Valkyrie");
  let enemy = new Hero("Voldemort");

  test("1. should hero do damage to enemy after hit", () => {
    hero.attack(enemy);

    expect(enemy.health).toBe(99);
  });

  test("2. should enemy do damage to hero after hit", () => {
    enemy.attack(hero);

    expect(hero.health).toBe(99); // ✅ Prawidłowo przejdzie test

    expect(enemy.health).toBe(99); // ✅ Stan naszego oponenta nadal
    // pozostał bez zmian po poprzednim  teście "1.", a co by się stało gdyby
    // ktoś "na szybko" przekopiował asercję i nie podmienił nazwy zmiennej? 🤔
  });
});
