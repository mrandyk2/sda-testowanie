class Calculator {
  add = (a, b) => a + b;
  subtract = (a, b) => a - b;
  divide = (a, b) => a / b;
  multiply = (a, b) => a * b;
}

const kalkulator = new Calculator();

// Opis pierwszej grupy do której należy test w tym przypadku klasa "Calculator"
describe("class Calculator tests", () => {
  // Opis czym zajmuje się test
  test("add method should add two digits", () => {
    // GIVEN
    const calculator = new Calculator();
    const firstDigit = 0.1;
    const secondDigit = 0.2;

    // WHEN
    const addResult = calculator.add(firstDigit, secondDigit);

    // THEN
    expect(addResult).toEqual(firstDigit + secondDigit);
  });

  test.todo("subtract method should subtract two digits");
  test.todo("divide method should divide two digits");
  test.todo("multiply method should multiply two digits");
});

//* Zadanie dla ambitnych - rozszerzyć klasę o dodatkowe metody i ich testy:
// - modulo
// - potęga
// - pierwiastek
describe.skip("*", () => {
  test.todo("subtract method should subtract two digits");
  test.todo("divide method should divide two digits");
  test.todo("multiply method should multiply two digits");
});

//** Zadanie dla bardzo ambitnych
// W każdym teście stosujesz zmienną pomocniczą calculator, wykorzystując
// beforeEach spróbuj zrefaktorować istniejący kod by mieć tylko jedną współdzieloną zmienną
// odkomentuj skip'a i sprawdź co się stanie ;)
describe.skip("**", () => {
  let calculator;

  beforeEach(() => {
    calculator = new Calculator();
  });

  test("should first", () => {
    console.log("wynik: ", calculator.add(1, 2));

    // Czary mary :o
    calculator = undefined;

    // Ciekawe co tu będzie
    console.log("co tu będzie: ", calculator);
  });

  test("should second", () => {
    // Ciekawe czy czary mary tu też działa
    console.log("A tu co będzie: ", calculator);
  });
});
