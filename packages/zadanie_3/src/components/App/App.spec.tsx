import React from 'react';
import {render, fireEvent, screen} from '@testing-library/react'
import '@testing-library/jest-dom';

import { App } from './App';

describe('App component', () => {
  test('should increase counter after button increase clicked', () => {
    // given
    const component = render(<App />);

    // when
    fireEvent.click(screen.getByTestId('counter-increase'));

    // then
    screen.getByText('Counter: 1');
  });

  test.todo('should decrease counter after button decrease clicked');
  test.todo('should disable decrease button when counter is 0');
});
