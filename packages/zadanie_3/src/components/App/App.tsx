import React from 'react';

export const App = (): JSX.Element => {
  const [counter, setCounter] = React.useState(0);
  
  return <>
    <div className="">
      <button data-testid="counter-increase" type="button" onClick={() => setCounter((prev) => prev + 1)}>+</button>
    </div>
    <p>Counter: { counter }</p>
  </>
}
