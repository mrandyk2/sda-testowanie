class Calculator {
  add = (a, b) => a + b;
  subtract = (a, b) => a - b;
  divide = (a, b) => a / b;
  multiply = (a, b) => a * b;
}

const kalkulator = new Calculator();

// Opis pierwszej grupy do której należy test w tym przypadku klasa "Calculator"
describe("class Calculator tests", () => {
  // Opis czym zajmuje się test
  test("add method should add two digits", () => {
    expect(kalkulator.add(1, 2)).toEqual(3);
  });

  test.todo("subtract method should subtract two digits");
  test.todo("divide method should divide two digits");
  test.todo("multiply method should multiply two digits");
});

// Zadanie dla ambitnych - rozszerzyć klasę o dodatkowe metody i ich testy:
// - modulo
// - potęga
// - pierwiastek
describe.skip("*", () => {
  test.todo("subtract method should subtract two digits");
  test.todo("divide method should divide two digits");
  test.todo("multiply method should multiply two digits");
});
